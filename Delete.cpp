//
//  Delete.cpp
//  Problems
//
//  Created by Vahe Danielyan on 12/20/15.
//  Copyright © 2015 Vahe Danielyan. All rights reserved.
//
#include <iostream>
using namespace std;
int main()
{
    int n;
    string s1,s2;
    cin >> n;
    cin >> s1 >> s2;
    if(n % 2 == 0)
    {
        if(s1 == s2)
            cout << "Deletion succeeded" << endl;
        else
            cout << "Deletion failed" << endl;
    }
    else
    {
        for(int i = 0;i < s1.length();i++)
        {
            if(s1[i] == '1')
                s1[i] = '0';
            else
                s1[i] = '1';
        }
        if(s1 == s2)
            cout << "Deletion succeeded" << endl;
        else
            cout << "Deletion failed" << endl;
    }
    return  0;
}