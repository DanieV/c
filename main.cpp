#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

#define mp make_pair
struct coord {
    double x;
    double y;
    int set;
};
///////////////////////////////////////////////////////////////////////////////////////

coord c[1000];
vector<pair< int,pair<int,int> > > S[20000];
set<int> ::iterator it;
vector<int> p(1000);
vector< pair<int,int> > a[1000];
//////////////////////////////////////////////////////////////////////////////////////
int n;
double r;
bool path[1000][1000];
//////////////////////////////////////////////////////////////////////////////////////

bool operator<(coord c1,coord c2)
{
    return c1.set <= c2.set;
}

double dist(coord c1, coord c2)
{
    return (sqrt((c1.x - c2.x)*(c1.x - c2.x) + (c1.y - c2.y)*(c1.y - c2.y)));
}

int dsu_get(int v) {
    return (v == p[v]) ? v : (p[v] = dsu_get(p[v]));
}

void dsu_unite(int a, int b) {
    a = dsu_get(a);
    b = dsu_get(b);
    if (a != b)
        p[a] = b;
}
///////////////////////////////////////////////////////////////////////////////////////

int main()
{
    cin >> n >> r;
    for (int i = 0; i < n; i++)
    {
        cin >> c[i].x >> c[i].y;
        c[i].set = i;
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i == j)
                continue;
            if ((dist(c[i], c[j]) <= r && c[i].set != c[j].set) || (c[i].set == c[j].set && path[i][j] == 0))
            {
                path[i][j] = 1;
                path[j][i] = 1;
                S[c[i].set].push_back(mp(dist(c[i], c[j]), mp(i, j)));
                c[j].set = c[i].set;
            }
        }
    }
    int q = 0;
    sort(c,c+n);
    for(int i = 1;i < n;i++)
    {
            while(c[i].set == c[i-1].set)
            {
                a[q].push_back(mp(c[i].x,c[i].y));
                i++;
            }
        q++;
    }
    int answ1 = 0, answ2 = 0;
    for (int j = 0; j < n; j++)
    {
        if (S[j].size() == 0)
        {
            continue;
        }
        answ1++;
        int cost = 0;
        vector < pair<int, int> > res;
        
        sort(S[j].begin(), S[j].end());
        p.resize(n);
        for (int i = 0; i < n; ++i)
            p[i] = i;
        for (int i = 0; i < S[j].size(); ++i) {
            int a = S[j][i].second.first, b = S[j][i].second.second, l = S[j][i].first;
            if (dsu_get(a) != dsu_get(b)) {
                cost += l;
                res.push_back(S[j][i].second);
                dsu_unite(a, b);
            }
        }
        answ2 += cost;
    }
    cout << answ1 << " " << answ2 << endl;
    return 0;
}